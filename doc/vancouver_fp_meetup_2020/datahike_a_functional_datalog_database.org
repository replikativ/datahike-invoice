#+Title: Datahike - A functional Datalog database
#+Author: Christian Weilbach
#+Email: christian@lambdaforge.io

#+OPTIONS: reveal_center:t reveal_progress:t reveal_history:t reveal_control:t
#+OPTIONS: reveal_mathjax:t reveal_rolling_Links:t reveal_keyboard:t reveal_overview:t num:nil
#+OPTIONS: reveal_slide_number:t
# +OPTIONS: reveal_width:1420 reveal_height:1080
#+OPTIONS: toc:nil
#+REVEAL_MARGIN: 0.1
#+REVEAL_MIN_SCALE: 0.6
#+REVEAL_MAX_SCALE: 1.2
#+REVEAL_TRANS: linear
#+REVEAL_THEME: sky
#+REVEAL_HLEVEL: 1
#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="">

* About me
  - PhD student in probabilistic programming at UBC
  - initial author of http://github.com/replikativ/ libraries
  - founding member of http://lambdaforge.io


* Datalog
  - subset of /Prolog/
  - similar to SQL (/relational algebra/)
  - but *supports recursion*
  - /implicit/ joining through /logic variables/
 
* Datalog Example
  #+BEGIN_SRC clojure
  [:find ?e ?senior 
   :where 
   [?e :name "Peter"]
   [?e :experience ?a]
   [(is-programming ?e "Clojure")]
   [(> ?a 5) ?senior]]
  #+END_SRC
	
	
* Datahike
  $\text{datahike} \approx \text{datascript} + \text{hitchhiker-tree}$
  
 
** Datomic Design
   - Rich Hickey: [[https://www.youtube.com/watch?v=V6DKjEbdYos][The Database as a Value]]
   - run query engine on /distributed snapshots/
   - schema only for /each relation/ (not table shapes)
   - every attribute is /optional/ (nilable)
   - query engine can /join over separate databases/ 
   - keep history for an *auditable* system
   - allow *direct access* to the immutable indices for batch processing systems

** Datomic Indices
   - store triples in 3 indices of /persistent trees/: EAVT, AEVT, AVET (no tables)
   https://tonsky.me/blog/unofficial-guide-to-datomic-internals/eavt@2x.png
   https://tonsky.me/blog/unofficial-guide-to-datomic-internals/
  

** DataScript 💘
   - in-memory datalog implementation for Clojure/Script
   - very /mature/ (5 years+ development)
   - faster in-memory query engine than Datomic
   - supports a lot of Datomic's datalog flavor:
     + pull-expressions
     + transactor functions
   - but has *opt-in* Schema
   - limitation: local, single-process implementation

** hitchhiker-tree
   - /fractal/ tree
   - append-log $\leftrightarrow$ B+-tree
   - $\Rightarrow$ optimal trade-off between read and write behaviour
   - "amortized" inserts and still $O(\log(n))$ reads
   - batched inserts
   - https://blog.datopia.io/2018/11/03/hitchhiker-tree/
	 

** hitchhiker-tree
   file:hhtree2.png
   

  
* Invoicing Example
  - REPL demo of https://gitlab.com/replikativ/datahike-invoice

  
* References
  - https://github.com/replikativ/datahike
  - HH-Tree talk: https://www.youtube.com/watch?v=jdn617M3-P4
  - DataScript internals: http://tonsky.me/blog/datascript-internals/
  - Reactive frontend vision: http://tonsky.me/blog/the-web-after-tomorrow/

    
